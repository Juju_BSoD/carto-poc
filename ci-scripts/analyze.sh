#!/bin/bash

run_cmd() {
  if eval "$@"; then
    echo -e "\e[32mOK $*"
  else
    echo -e "\e[31mFAIL $*"
    exit 1
  fi
}

echo "--------------- analyze stage ---------------"

run_cmd ./mvnw sonar:sonar \
  -Dsonar.projectKey=$SONAR_PROJECT_KEY \
  -Dsonar.host.url=$SONAR_SRV_URL \
  -Dsonar.login=$SONAR_KEY \
  -Dsonar.java.binaries=target/classes/fr/ngms/ \
  -Dsonar.sources=src/main/java,webapp/src \
  -Dsonar.exclusions=webapp/src/assets,webapp/dist,webapp/node,webapp/src/app/**/*.spec.ts \
  -Dsonar.tests=src/test/java \
  -Dsonar.dynamicAnalysis=reuseReports \
  -Dsonar.junit.reportsPath=target/surefire-reports \
  -Dsonar.java.coveragePlugin=jacoco \
  -Dsonar.coverage.exclusions=src/main/java/fr/ngms/cartopoc/orm/**,src/main/java/fr/ngms/cartopoc/models/**,src/main/java/fr/ngms/cartopoc/core/**,src/main/java/fr/ngms/cartopoc/security/utils/** \
  -Dsonar.coverage.jacoco.xmlReportPaths=target/site/jacoco/jacoco.xml
  
# Get analysis result
echo "url to check: $SONAR_SRV_URL/api/qualitygates/project_status?projectKey=$SONAR_PROJECT_KEY"
project_status=$(curl -u $SONAR_KEY: -s $SONAR_SRV_URL/api/qualitygates/project_status?projectKey=$SONAR_PROJECT_KEY | awk -F'"' '{print $6}')
if [ "$project_status" = "OK" ]; then
  echo -e "\e[32mOK $*"
else
  echo "Quality Gate: $project_status"
  echo "Results: $SONAR_SRV_URL/api/qualitygates/project_status?projectKey=$SONAR_PROJECT_KEY"
  echo -e "\e[31mFAIL $*"
  exit 1
fi
