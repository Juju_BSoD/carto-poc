package fr.ngms.cartopoc.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.basic.BasicPOIPayload;
import fr.ngms.cartopoc.models.user.AuthResponse;
import fr.ngms.cartopoc.test.helpers.DatabaseCleaner;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.sql.SQLException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Class which contains series of tests to check routes of POIController, with mock geojson data
 */
@QuarkusTest
@Tag("integration")
class POIControllerTest {

    private static String token;

    @BeforeAll
    public static void setup() throws SQLException {
        DatabaseCleaner.clean();
        DatabaseCleaner.setup();
        var authResponse = given()
                .port(ConfigProvider.getConfig()
                        .getValue("quarkus.http.test-port", Integer.class))
                .body("{\n" +
                        "    \"email\" : \"admin@admin\",\n" +
                        "    \"password\": \"admin\"\n" +
                        "}")
                .contentType(ContentType.JSON).post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        token = authResponse.payload.getToken();
    }

    @AfterAll
    public static void clean() throws SQLException {
        DatabaseCleaner.clean();
    }


    @Test
    void shouldReturnBadRequestWhenIDisNegative() {
        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .when()
                .delete("/poi/delete/-14")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat()
                .body(is("Id of POI cannot be negative"));

        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .when()
                .put("/poi/update/-1")
                .then()
                .statusCode(400)
                .assertThat()
                .body(is("Id of POI cannot be negative"));
    }

    @Test
    void shouldReturnBadRequestWhenPOIInformationIsMissing() {
        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{}")
                .when()
                .put("/poi/update/2")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{ }")
                .when()
                .put("/poi/update/5")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }


    @Test
    void shouldReturnOKWhenUpdatePOI() {

        var response = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"posX\" : 117.6,\n" +
                        "  \"name\": \"Bologne catedral\",\n" +
                        "  \"posY\" : 10.6,\n" +
                        "  \"attributesData\": {\n" +
                        "    \"lieu\": \"batiment public\"\n" +
                        "  }\n" +
                        "}")
                .when()
                .put("/poi/update/12")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }


    @Test
    void shouldReturnBadRequestWhenUpdateWithEmptyPOIInformation() {

        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"posX\" : ,\n" +
                        "  \"name\": \"Bologne catedral\",\n" +
                        "  \"posY\" : 10.6,\n" +
                        "  \"attributesData\": {\n" +
                        "    \"lieu\": \"batiment public\"\n" +
                        "  }\n" +
                        "}")
                .when()
                .put("/poi/update/12")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    void shouldReturnMessageWhenUpdateUnknownPOI() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"posX\" : 142.9,\n" +
                        "  \"name\": \"Hydrant\",\n" +
                        "  \"posY\" : 11.6,\n" +
                        "  \"attributesData\": {\n" +
                        "    \"lieu\": \"objet incendie\"\n" +
                        "  }\n" +
                        "}")
                .when()
                .put("/poi/update/42")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertEquals("POI with id (42) not exist", response.message);
        });
    }


    @Test
    void shouldReturnOKWhenUpdateSamePOIThreeTimes() {
        var responseUpdateOne = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"posX\" : 188.9,\n" +
                        "  \"name\": \"Madrid Police station\",\n" +
                        "  \"posY\" : 21.4,\n" +
                        "  \"attributesData\": {\n" +
                        "    \"lieu\": \"batiment public\"\n" +
                        "  }\n" +
                        "}")
                .when()
                .put("/poi/update/6")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        var responseUpdateTwo = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"posX\" : 144.9,\n" +
                        "  \"name\": \"Paris Police station\",\n" +
                        "  \"posY\" : 17.4,\n" +
                        "  \"attributesData\": {\n" +
                        "    \"lieu\": \"batiment public\"\n" +
                        "  }\n" +
                        "}")
                .when()
                .put("/poi/update/6")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        var responseUpdateThree = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"posX\" : 101.9,\n" +
                        "  \"name\": \"Lisbon Police station\",\n" +
                        "  \"posY\" : 87.4,\n" +
                        "  \"attributesData\": {\n" +
                        "    \"lieu\": \"batiment public\"\n" +
                        "  }\n" +
                        "}")
                .when()
                .put("/poi/update/6")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        assertAll(() -> {
            assertTrue(responseUpdateOne.sucess);
            assertTrue(responseUpdateTwo.sucess);
            assertTrue(responseUpdateThree.sucess);

            assertEquals(6, responseUpdateThree.payload.getId());
            assertEquals("Madrid Police station", responseUpdateOne.payload.getName());
            assertEquals("Paris Police station", responseUpdateTwo.payload.getName());
            assertEquals("Lisbon Police station", responseUpdateThree.payload.getName());

        });

    }


    @Test
    void shouldReturnOKWhenUpdatePOIALot() {
        for (var i = 0; i < 500; i++) {
            var response = given()
                    .header("Authorization", "Bearer " + token)
                    .contentType(ContentType.JSON)
                    .body("{\n" +
                            "  \"posX\" : 142.9,\n" +
                            "  \"name\": \"Hydrant\",\n" +
                            "  \"posY\" : 11.6,\n" +
                            "  \"attributesData\": {\n" +
                            "    \"lieu\": \"objet incendie\"\n" +
                            "  }\n" +
                            "}")
                    .when()
                    .put("/poi/update/8")
                    .then()
                    .statusCode(Response.Status.OK.getStatusCode())
                    .assertThat();
        }
    }


    @Test
    void shouldReturnOKWhenDeletePOI() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/poi/delete/7")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        assertAll(() -> {
            assertTrue(response.sucess);
            assertNull(response.message);
            assertNull(response.payload);
        });

    }

    @Test
    void shouldReturnOKWhenDeleteSeveralPOI() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/poi/delete/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        assertAll(() -> {
            assertTrue(response.sucess);
            assertNull(response.message);
            assertNull(response.payload);
        });

        var response2 = given()
                .when()
                .header("Authorization", "Bearer " + token)
                .delete("/poi/delete/9")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        assertAll(() -> {
            assertTrue(response2.sucess);
            assertNull(response2.message);
            assertNull(response2.payload);
        });

        var response3 = given()
                .when()
                .header("Authorization", "Bearer " + token)
                .delete("/poi/delete/13")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        assertAll(() -> {
            assertTrue(response3.sucess);
            assertNull(response3.message);
            assertNull(response3.payload);
        });

    }


    @Test
    void shouldReturnMessageWhenDeleteUnknownPOI() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/poi/delete/29")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertEquals("POI with id (29) not exist", response.message);
        });
    }

    @Test
    void shouldReturnMessageWhenDeletePOIAlreadyDeleted() {
        var response = given()
                .when()
                .header("Authorization", "Bearer " + token)
                .delete("/poi/delete/13")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat()
                .extract()
                .as(basicPOIPayloadResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertEquals("POI with id (13) not exist", response.message);
            assertNull(response.payload);
        });
    }

    @Test
    void shouldNotAuthorizeUserDeleteAndUpdatePoi(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"user@user\",\n" +
                        "    \"password\": \"user\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                .when()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + tmpToken)
                .delete("/poi/delete/13")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
        given()
                .when()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + tmpToken)
                .put("/poi/update/13")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
        given()
                .when()
                .contentType(ContentType.JSON)
                .put("/poi/update/13")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
        given()
                .when()
                .contentType(ContentType.JSON)
                .delete("/poi/delete/13")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }

    @Test
    void shoudAuthorizeEditorUpdateAndDeletePoi(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"editor@editor\",\n" +
                        "    \"password\": \"editor\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                .header("Authorization", "Bearer " + tmpToken)
                .when()
                .delete("/poi/delete/5")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"posX\" : 142.9,\n" +
                        "  \"name\": \"Hydrant\",\n" +
                        "  \"posY\" : 11.6,\n" +
                        "  \"attributesData\": {\n" +
                        "    \"lieu\": \"objet incendie\"\n" +
                        "  }\n" +
                        "}")
                .when()
                .put("/poi/update/8")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

    }


    static class BasicPOIPayloadResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        BasicPOIPayload payload;
    }

    @NotNull
    static TypeRef<BasicPOIPayloadResponse> basicPOIPayloadResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    @NotNull
    static TypeRef<AuthResponseResponse> authResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }
    static class AuthResponseResponse implements Serializable {
        @JsonProperty("message")
        String message;

        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("payload")
        AuthResponse payload;
    }
}
