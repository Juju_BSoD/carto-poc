package fr.ngms.cartopoc.test;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@Tag("integration")
class HealthCheckTests {

  @Test
  void statusEndpointTest() {
    given()
      .when().get("/health")
      .then()
      .statusCode(Response.Status.OK.getStatusCode())
      .body(is("alive"));
  }
}