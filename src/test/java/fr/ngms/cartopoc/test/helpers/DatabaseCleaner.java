package fr.ngms.cartopoc.test.helpers;

import org.h2.tools.RunScript;

import java.io.InputStreamReader;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashSet;

public class DatabaseCleaner {
  public static void setup() {
    var url = "jdbc:h2:mem:test-db";
    var login = "test";
    var pass = "test";
    var sqlFile = DatabaseCleaner.class.getResourceAsStream("/test-db.sql");

    try (var con = DriverManager.getConnection(url, login, pass)) {
      RunScript.execute(con, new InputStreamReader(sqlFile));
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
  }

  public static void clean() throws SQLException {
    var url = "jdbc:h2:mem:test-db";
    var login = "test";
    var pass = "test";

    var connection = DriverManager.getConnection(url, login, pass);
    var s = connection.createStatement();

    // Disable FK
    s.execute("SET REFERENTIAL_INTEGRITY FALSE");

    // Find all tables and truncate them
    var tables = new HashSet<String>();
    var rs = s.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA='PUBLIC'");
    while (rs.next()) {
      tables.add(rs.getString(1));
    }
    rs.close();
    for (String table : tables) {
      s.executeUpdate("TRUNCATE TABLE " + table);
    }

    // Idem for sequences
    var sequences = new HashSet<String>();
    rs = s.executeQuery("SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='PUBLIC'");
    while (rs.next()) {
      sequences.add(rs.getString(1));
    }
    rs.close();
    for (String seq : sequences) {
      s.executeUpdate("ALTER SEQUENCE " + seq + " RESTART WITH 1");
    }

    // Enable FK
    s.execute("SET REFERENTIAL_INTEGRITY TRUE");
    s.close();
    connection.close();
  }
}
