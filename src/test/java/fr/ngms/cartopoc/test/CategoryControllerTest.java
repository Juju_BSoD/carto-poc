package fr.ngms.cartopoc.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.responses.basic.BasicCategoryPayload;
import fr.ngms.cartopoc.models.responses.basic.BasicCategoryPayload.CategoryInformation;
import fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload;
import fr.ngms.cartopoc.models.user.AuthResponse;
import fr.ngms.cartopoc.test.helpers.DatabaseCleaner;
import io.quarkus.test.Mock;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
@Tag("integration")
class CategoryControllerTest {

    private static String token;

    @BeforeAll
    public static void setup() throws SQLException {
        DatabaseCleaner.clean();
        DatabaseCleaner.setup();
        var authResponse = given().
                port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class))
                .body("{\n" +
                "    \"email\" : \"admin@admin\",\n" +
                "    \"password\": \"admin\"\n" +
                "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        token = authResponse.payload.getToken();
    }

    @AfterAll
    public static void clean() throws SQLException {
        DatabaseCleaner.clean();
    }


    @Test
    void testGetAllCategories() {
        var categories = given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicCategoryPayload());
        assertTrue(categories.payload.getCategories().size() > 0);
    }


    @Test
    void testFindCategoryById() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(categoryInformationResponse());

        assertTrue(response.sucess);
        assertNull(response.message);
        assertAll(() -> {
            assertEquals(1, response.payload.getId());
            assertEquals("camera", response.payload.getName());
            assertEquals("categorie des camera", response.payload.getDescription());
        });
    }

    @Test
    void testFindUnknownCategoryById() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories/10")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .assertThat()
                .extract()
                .as(basicCategoryPayload());

        assertFalse(response.sucess);
        assertNotNull(response.message);
        assertEquals("Category with id (10) not exist", response.message);
    }

    @Test
    void testFindUnknownCategoryByIdBadRequest() {
        given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories/-1")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body(is("id must be greater than 0 not"))
                .assertThat();
    }


    @Test
    void testCreateAlreadyExistCategory() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"camera\",\n" +
                        "    \"description\": \"category 1\"\n" +
                        "}")
                .when()
                .post("/categories/create")
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .assertThat()
                .extract()
                .as(categoryInformationResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertEquals("Category with name (camera) already exist", response.message);
        });
    }

    @Test
    void testCreateCategory() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"category_9\",\n" +
                        "    \"description\": \"category 9\"\n" +
                        "}")
                .when()
                .post("/categories/create")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode())
                .assertThat()
                .extract()
                .as(categoryInformationResponse());

        assertAll(() -> {
            assertEquals(9, response.payload.getId());
            assertEquals("category_9", response.payload.getName());
            assertEquals("category 9", response.payload.getDescription());
        });

        var categories = given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicCategoryPayload());

        assertAll(() -> assertEquals(9, categories.payload.getCategories().size()));
    }

    @Test
    void testCreateCategoryWithBlankValues() {
        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"    \",\n" +
                        "    \"description\": \"    \"\n" +
                        "}")
                .when()
                .post("/categories/create")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    void testCreateCategoryWithBlankDescription() {
        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"dqsd\",\n" +
                        "    \"description\": \"    \"\n" +
                        "}")
                .when()
                .post("/categories/create")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }


    @Test
    void testCreateCategoryWithBlanName() {
        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"    \",\n" +
                        "    \"description\": \"zssq\"\n" +
                        "}")
                .when()
                .post("/categories/create")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    void testCreateCategoryWithInvalidBody() {
        given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("dqskldjli")
                .when()
                .post("/categories/create")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    void testUpdateCategory() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"category_3_upd\",\n" +
                        "    \"description\": \"category 3 upd\"\n" +
                        "}")
                .when()
                .put("/categories/update/3")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(categoryInformationResponse());
        assertAll(() -> {
            assertEquals(3, response.payload.getId());
            assertEquals("category_3_upd", response.payload.getName());
            assertEquals("category 3 upd", response.payload.getDescription());
        });
    }

    @Test
    void testDeleteCategory() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/categories/delete/4")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicCategoryPayload());

        assertAll(() -> {
            assertTrue(response.sucess);
            assertNull(response.message);
        });

        var categories = given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicCategoryPayload());

        assertAll(() -> assertEquals(9, categories.payload.getCategories().size()));
    }


    @Test
    void testDeleteUnknownCategory() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/categories/delete/16")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat()
                .extract()
                .as(basicCategoryPayload());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertEquals("Category with id (16) not exist", response.message);
        });
    }

    @Test
    void testUpdateUnknownCategory() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"category_13_upd\",\n" +
                        "    \"description\": \"category 13 upd\"\n" +
                        "}")
                .when()
                .put("/categories/update/13")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat()
                .extract()
                .as(categoryInformationResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertEquals("Category with id (13) not exist", response.message);
        });
    }

    @Test
    void testGetCategoryLayer() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories/1/layers")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(layerListResponse());

        assertTrue(response.sucess);
        assertNull(response.message);

        var layers = response.payload.getLayers();

        var first = layers.stream().filter(l -> l.getId() == 1).findFirst().get();

        assertAll(() -> {
            assertEquals(2, layers.size());
            assertEquals("layer_1_camera", first.getName());
            assertEquals(1, first.getId());
            assertEquals("couche des camera normal", first.getDescription());
            assertEquals(List.of("marque", "type"), first.getProperties());
        });
    }

    @Test
    void testGetEmptyCategoryLayer() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories/3/layers")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(layerListResponse());

        assertTrue(response.sucess);
        assertNull(response.message);
        var layers = response.payload.getLayers();

        assertAll(() -> assertEquals(0, layers.size()));
    }

    @Test
    void testGetLayerOfUnknownCategory() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when().get("/categories/25/layers")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat()
                .extract()
                .as(layerListResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertNotNull(response.message);
            assertEquals("Category with id (25) not exist", response.message);
        });
    }

    @Test
    void testGetCategoryNotAuthenticated(){
        given()
                .when().get("/categories")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }
    @Test
    void testUpdateCategoryWithUserNotAllowed(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"user@user\",\n" +
                        "    \"password\": \"user\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"category_3_upd\",\n" +
                        "    \"description\": \"category 3 upd\"\n" +
                        "}")
                .when()
                .put("/categories/update/3")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }

    @Test
    void testAddCategoryUserNotAllowed(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"user@user\",\n" +
                        "    \"password\": \"user\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"Test Categori\",\n" +
                        "    \"description\": \"zssq\"\n" +
                        "}")
                .when()
                .post("/categories/create")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                .assertThat();
    }
    @Test
    void testDeleteCategoryUserNotAllowed(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"editor@editor\",\n" +
                        "    \"password\": \"editor\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                    .header("Authorization", "Bearer " + tmpToken)
                    .when()
                    .delete("/categories/delete/4")
                    .then()
                    .statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }

    @Test
    void testUpdateCategoryWithEditorRole(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"editor@editor\",\n" +
                        "    \"password\": \"editor\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();

        given()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"category_3_upd_2\",\n" +
                        "    \"description\": \"category 3 upd\"\n" +
                        "}")
                .when()
                .put("/categories/update/3")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(categoryInformationResponse());

    }

    @Test
    void testGetCategoryBadToken(){
        var badToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJuZ21zIiwiaWF0IjoxNjE0NTIyNzY4LCJleHAiOjE2NDYwNTg3NjgsImF1ZCI6Ind"+
                        "3dy5leGFtcGxlLmNvbSIsInN1YiI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJHaXZlbk5hbWUiOiJKb2hubnkiLCJTdXJuYW1lIjoiUm9ja"+
                        "2V0IiwiRW1haWwiOiJqcm9ja2V0QGV4YW1wbGUuY29tIiwiUm9sZSI6WyJNYW5hZ2VyIiwiUHJvamVjdCBBZG1pbmlzdHJhdG9yIl19"+
                ".A26ZuO1vNX1zHSYxhnhpcwfDW5ogtQOSWXuJSqZPqVY";
        given()
                .header("Authorization", "Bearer " + badToken)
                .when().get("/categories")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }


    @NotNull
    static TypeRef<CategoryInformationResponse> categoryInformationResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    @NotNull
    static TypeRef<BasicCategoryPayloadResponse> basicCategoryPayload() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }


    @NotNull
    static TypeRef<BasicLayerResponse> layerListResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    @NotNull
    static TypeRef<AuthResponseResponse> authResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    static class CategoryInformationResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        CategoryInformation payload;
    }

    static class BasicLayerResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        BasicLayersPayload payload;
    }

    static class BasicCategoryPayloadResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        BasicCategoryPayload payload;
    }
    static class AuthResponseResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        AuthResponse payload;
    }
}