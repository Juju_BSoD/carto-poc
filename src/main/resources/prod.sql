--
-- Data for Name: ngms_categories; Type: TABLE DATA; Schema: public; Owner: root
--
INSERT INTO ngms_categories (id, description, name) VALUES (1, 'Category wich contains deleted layers and other layers without specified category', 'Other');
INSERT INTO ngms_categories (id, description, name) VALUES (2, 'Radars qui flash lors du dépassement de la vitesse autorisé par un véhicule', 'Radars fixe europe');
INSERT INTO ngms_categories (id, description, name) VALUES (3, 'Elements de Police', 'Police');
INSERT INTO ngms_categories (id, description, name) VALUES (4, 'Etablissements hospitaliers en Ile de France', 'Etablissements Hopitaliers Ile de France');



--
-- Data for Name: ngms_layers; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (1, 'Radar fixe en France 130 ', 'vitesse autorisé', 'Radars fixe Français 130', 2);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (2, 'Radar fixe en France 80 ', 'vitesse autorisé', 'Radars fixe Français 80', 2);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (3, 'Commissariats de police en France ', 'Adresse', 'Comissariats de Police Français', 3);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (4, 'Etablissements Hospitaliers SEINE ET MARNE', 'Adresse', 'SEINE ET MARNE', 4);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (5, 'Etablissements Hospitaliers Paris', 'Adresse', 'Paris', 4);


--
-- Data for Name: ngms_poi; Type: TABLE DATA; Schema: public; Owner: root
--INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (5789,'DE Radar Feu Rouge DE',8.05314, 52.26739,10);

INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (1,'TOURCELLES CHAUMONT vers SEMIDE RD977',4.5778, 49.382,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (2,'DOUBLE SENS RD8043',5.09251, 49.67523,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (3,'DOUBLE SENS RN248',-0.52535, 46.24247,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (4,'NIORT VERS LA ROCHELLE RN11',-0.66783, 46.19021,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (5,'EPINAL VERS BESANCON RN57',6.30523, 47.72129,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (6,'ST MEEN LE GRAND VERS PLOERMEL RD766',-2.26267, 48.10143,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (7,'DOUBLE SENS RD560',5.86652, 43.46718,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (8,'DOUBLE SENS RN94',6.36334, 44.52951,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (9,'DOUBLE SENS RD43',6.04887, 43.38478,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (10,'DOUBLE SENS RD1001',2.14978, 49.48912,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (11,'DOUBLE SENS RD917',1.0304, 47.784,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (12,'DOUBLE SENS RD919',2.38441, 49.9603,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 13,'Albertville vers Annecy RD1508',6.27998, 45.7552,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 14,'Bonneville vers Marignier RD19G',6.4749, 46.071,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 15,'DOUBLE SENS RD925',6.1895, 45.526,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 16,'DOUBLE SENS RD5',7.5399, 48.387,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (17,'DOUBLE SENS RD1061',7.1083, 48.922,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 18,'DOUBLE SENS RN20',1.9423, 42.455,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 19,'DOUBLE SENS RD81',3.0212, 42.753,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 20,'DOUBLE SENS RN116',1.9606, 42.433,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 21,'DOUBLE SENS RD938',-0.2657, 43.239,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 22,'ST PEE SUR NIVELLE vers ST JEAN DE LUZ RD918',-1.5818, 43.36,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 23,'TAUVES vers ROCHEFORT MONTAGNE RD922',2.6957, 45.602,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 24,'VERDUN vers BAR LE DUC RD1916 - VOIE SACREE NATIONALE',5.1962, 48.784,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 25,'DOUBLE SENS RD417',5.6475, 47.9623,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 26,'Mortain vers St-Hilaire du Harcouët RD977',-1.0612, 48.6,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 27,'SAULGE L HOPITAL vers BRISSAC QUINCE RD761',-0.402958, 47.313826,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 28,'BEAUPREAU EN MAUGES vers ANGERS RD762',-0.912927, 47.248639,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 29,'DOUBLE SENS RD938',-0.10143, 47.5582,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 30,'MARCILLY EN VILLETTE vers MENESTREAU EN VILLETTE RD108',2.01479, 47.743,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 31,'DOUBLE SENS RD97',1.9673, 48.023,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 32,'MILLAU VERS PONT DE SALARS RD911',2.98771, 44.2327,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 33,'DOUBLE SENS RD9',3.91759, 45.27165,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 34,'LOUDES vers ST CHRISTOPHE SUR DOLAISON RD906',3.7753, 45.024,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 35,'DOUBLE SENS RD589',3.8397, 45.019,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 36,'DOUBLE SENS RD46',4.1481, 45.334,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 37,'DOUBLE SENS RD43',4.0761, 46.09,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 38,'TOURS vers VENDÔME RN10',1.0176, 47.718,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 39,'MONT DE MARSAN vers SAUGNACQ ET MURET RD834',-0.74267, 44.20029,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 40,'BLOIS vers TOURS RD751',1.3053, 47.562,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 41,'DOUBLE SENS RD34',-1.7224, 48.052,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 42,'Saugnacq et Muret vers Mont de Marsan RD834',-0.74267, 44.20029,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 43,'Vic Fezensac vers Auch RN124',0.356, 43.739,2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (44,'CLERMONT FERRAND vers BORDEAUX A89',1.897, 45.335,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (45,'BORDEAUX vers PERIGUEUX A89',0.483, 45.097,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (46,'BESANCON vers MONTBELIARD A36',6.47455, 47.40671,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (47,'Alençon vers Rouen A28',0.16232, 48.53353,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (48,'Rouen vers Alençon A28',0.17585, 48.56124,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (49,'Lille vers Paris A1',2.85223, 49.94314,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (50,'Paris vers Caen A13',0.5142, 49.3905,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (51,'LYON vers ORANGE A7',4.73481, 44.1952,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (52,'Chateauroux vers Vierzon A20',1.86836, 47.09909,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (53,'Lille vers Dunkerque A25',2.74673, 50.72106,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (54,'Tulle vers Clermont A89',2.04453, 45.43246,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (55,'Châteauroux vers Limoges A20',1.47534, 46.4287,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (56,'Bordeaux vers Toulouse A62',-0.31764, 44.5744,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (57,'Bâle vers Mulhouse A35',7.52692, 47.60741,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (58,'Colmar vers Mulhouse A35',7.37127, 47.97092,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (59,'FOIX vers TOULOUSE A66',1.63099, 43.19748,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (60,'LYON vers BOURG EN BRESSE A42',5.14823, 45.86378,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (61,'LE MANS VERS PARIS A11',0.23942, 48.05751,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (62,'PARIS VERS BORDEAUX A10',0.28586, 46.56029,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (63,'LE MANS VERS RENNES A81',-0.46373, 48.05173,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (64,'ORANGE vers SALON DE PROVENCE A7',4.88956, 44.01694,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (65,'toulouse vers paris A20',1.62776, 46.75071,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (66,'CAEN VERS RENNES A84',-1.00797, 48.95187,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (67,'PARIS VERS LIMOGES A20',1.62297, 46.81699,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (68,'LYON VERS PARIS A6',3.73158, 47.73905,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (69,'La Ville aux Dames vers Tours -',0.74247, 47.39722,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (70,'Tours vers la Ville aux Dames -',0.74129, 47.39703,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (71,'Lorient vers Rennes -',-1.75776, 48.09839,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (72,'RENNES vers CHARTRES DE BRETAGNE -',-1.69948, 48.06301,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (73,'Rennes vers Lorient -',-1.75759, 48.099,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 77,'Lorient vers Rennes -',-1.75419, 48.10138,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 78,'Ouest vers Est -',1.24417, 48.75866,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 79,'Nord vers Sud -',-4.40631, 48.40188,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 80,'Louviers vers Heudebouville -',1.18436, 49.20748,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 81,'Est vers Ouest -',1.16164, 49.01643,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 82,'Sud-Ouest vers Nord-Est -',1.11881, 49.01918,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 83,'Trémuson vers St Brieuc -',-2.77898, 48.51745,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 84,'Trégueux vers St Brieuc -',-2.74855, 48.49254,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 85,'Vers le centre ville -',5.051, 47.34155,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 86,'Vers l-extérieur du centre ville -',5.05173, 47.34108,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 87,'Vers le centre ville -',5.04453, 47.31603,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 88,'Vers le centre ville -',5.01383, 47.31374,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 89,'Rue Charles Dumont vers Rue des Moulins -',5.03785, 47.30932,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 90,'Corcelles les Monts vers le centre ville -',5.01435, 47.3135,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 91,'SAINT APOLLINAIRE vers FONTAINE LES DIJON -',5.05718, 47.33656,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 92,'Rue de Longvic vers Dijon -',5.04973, 47.30522,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 93,'FONTAINE LES DIJON vers SAINT APOLLINAIRE -',5.06105, 47.33623,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES ( 94,'Bourges vers Fussy -',2.40934, 47.10315,1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (95,'Commissariat de police d-Oyonnax',5.65029996, 46.25760162,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (96,'Commissariat de police de Bourg en Bresse',5.22517643, 46.20676468,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (97,'Commissariat de police de Château Thierry',3.38927094, 49.04801921,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (98,'Commissariat de police de Laon',3.62633491, 49.56982361,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (99,'Commissariat de police de Saint Quentin',3.29671879, 49.84262881,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (100,'Commissariat de police de Soissons',3.32305622, 49.38573841,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (101,'Commissariat de police de Tergnier',3.29299764, 49.65555538,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (108,'Commissariat de police de Montluçon',2.59599273, 46.34081015,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (109,'Commissariat de police de Moulins',3.32743809, 46.56800268,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (110,'Commissariat de police de Vichy',3.42401274, 46.12779466,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (111,'Commissariat de police de Digne-les-bains',6.23499689, 44.09321411,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (112,'Commissariat de police de Manosque',5.78616988, 43.83295915,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (113,'Commissariat de police de Briançon',6.63800505, 44.89787608,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (114,'Commissariat de police de Gap',6.08096577, 44.560405,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (115,'Commissariat de police d-Antibes',7.11988475, 43.57987588,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (116,'Commissariat de police de Beausoleil',7.42510061, 43.74291348,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (117,'Commissariat de police de Cagnes-sur-Mer',7.15040689, 43.65602164,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (118,'Commissariat de police de Cannes',7.01669032, 43.55402931,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (119,'Commissariat de police de Cannes la Bocca',6.97951402, 43.5495393,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (120,'Commissariat de police de Grasse',6.92263861, 43.65051036,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (121,'Commissariat de police de Le Cannet',7.01611259, 43.57309944,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (122,'Commissariat de police de Menton',7.4983561, 43.7749352,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (123,'Commissariat de police de Nice',7.27266282, 43.70361279,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (124,'Commissariat de police de Nice - Secteur Ariane',7.30516396, 43.73791788,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (125,'Commissariat de police de Nice - Secteur Saint Augustin',7.20479974, 43.67797918,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (126,'Commissariat de police de Roquebrune-Cap-Martin',7.47069745, 43.76096281,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (127,'Commissariat de police de Saint-Laurent-du-Var',7.19351514, 43.67178022,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (128,'Commissariat de police de Vallauris',7.04851819, 43.57512797,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (129,'Commissariat de police de Villefranche-sur-Mer',7.30998958, 43.70526304,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (130,'Commissariat de police d-Aubenas',4.39427863, 44.61589218,3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (131,'Etablissement Réadaptation Fonctionnelle | CENTRE DE READAPTATION FONCTIONNELLE ELLEN POIDATZ 77310 ST FARGEAU PONTHIERRY',2.5246073, 48.5542849,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (132,'Centre Médico-Psychologique (C.M.P.) | DISPENSAIRE D HYGIENE MENTALE SECTEUR PSYCHIATRIQUE NO: 6 77290 MITRY MORY',2.622898, 48.9790984,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (133,'Etablissement de Soins Longue Durée | LONG SEJOUR DU C.H.DE MELUN 77000 MELUN',2.6661942, 48.5386165,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (134,'Centre Médico-Psychologique (C.M.P.) |  77250 MORET SUR LOING',2.8151404, 48.3714803,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (135,'Centre Hospitalier (C.H.) | CENTRE HOSPITALIER DE MEAUX 77104 MEAUX CEDEX',2.8841968, 48.9672432,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (136,'Centre Médico-Psychologique (C.M.P.) | DISPENSAIRE D HYGIENE MENTALE SECTEUR PSYCHIATRIQUE NO: 6 77100 MEAUX',2.9105034, 48.9513558,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (137,'Centre d Accueil Thérapeutique à temps partiel (C.A.T.T.P.) |  77130 MONTEREAU FAULT YONNE',2.956193, 48.384734,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (138,'Centre d Accueil Thérapeutique à temps partiel (C.A.T.T.P.) | CENTRE D ACCUEIL THERAPEUTIQUE A TEMPS PARTIEL 77I04 77120 COULOMMIERS',3.0827256, 48.8097014,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (139,'Centre Médico-Psychologique (C.M.P.) |  77260 LA FERTE SOUS JOUARRE',3.1160352, 48.9690536,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (140,'Etablissement de Soins Chirurgicaux |  77190 DAMMARIE LES LYS',2.6403974, 48.5149485,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (141,'Centre Hospitalier (C.H.) | CENTRE HOSPITALIER DE FONTAINEBLEAU 77305 FONTAINEBLEAU CEDEX',2.6965035, 48.4118966,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (142,'Etablissement Réadaptation Fonctionnelle |  77170 COUBERT',2.7179443, 48.680505,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (143,'Centre Hospitalier (C.H.) |  77165 ST SOUPPLETS',2.8170911, 49.0419975,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (144,'Centre Hospitalier (C.H.) |  77104 MEAUX CEDEX',2.8696165, 48.9558243,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (145,'Etablissement de Convalescence et de Repos |  77100 MAREUIL LES MEAUX',2.875064, 48.937529,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (146,'Centre Médico-Psychologique (C.M.P.) | DISPENSAIRE D HYGIENE SOCIALE 77100 MEAUX',2.8966667, 48.9465009,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (147,'Centre Médico-Psychologique (C.M.P.) |  77176 SAVIGNY LE TEMPLE',2.5835, 48.5945621,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (148,'Centre Médico-Psychologique (C.M.P.) | DISPENSAIRE D HYGIENE MENTALE, 77270 VILLEPARISIS',2.6051393, 48.9503954,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (149,'Etablissement de Soins Pluridisciplinaire | HOPITAL PRIVE DE MARNE CHANTEREINE (H.P.M.C.) 77177 BROU SUR CHANTEREINE',2.629476, 48.88407,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (150,'Centre Hospitalier (C.H.) | HOPITAL DE JOUR POUR ENFANTS  DE L ARCHE GUEDON 77200 TORCY',2.6365043, 48.8519453,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (151,'Centre Médico-Psychologique (C.M.P.) | DISPENSAIRE D HYGIENE MENTALE PSYCHIATRIE INFANTO JUVENILE 4 77230 DAMMARTIN EN GOELE',2.6765277, 49.0537453,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (152,'Centre Hospitalier (C.H.) |  77100 MEAUX',2.8832435, 48.9650627,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (153,'Centre d Accueil Thérapeutique à temps partiel (C.A.T.T.P.) |  77130 MONTEREAU FAULT YONNE',2.9590533, 48.4223649,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (154,'Centre Médico-Psychologique (C.M.P.) |  77370 NANGIS',3.0118719, 48.5572487,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (155,'Centre Hospitalier (C.H.) |  77120 COULOMMIERS',3.086031, 48.8132824,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (156,'Etablissement Réadaptation Fonctionnelle | CENTRE DE REED. FONCTIONNELLE ET DE READ. FONCTIONNELLE 77310 BOISSISE LE ROI',2.572056, 48.5265518,4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (157,'Etablissement de Soins Pluridisciplinaire | HOPITAL HENRY DUNANT  DE LA CROIX ROUGE FRANCAISE 75016 PARIS',2.2581138, 48.8402399,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (158,'Etablissement de Soins Pluridisciplinaire | CLINIQUE MEDICALE ET PEDAGOGIQUE EDOUARD RIST 75016 PARIS',2.2652721, 48.8464867,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (159,'Etablissement de Soins Pluridisciplinaire | CLINIQUE SAINTE-THERESE DE L ENFANT JESUS 75017 PARIS',2.3024255, 48.886526,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (160,'Centre Hospitalier Spécialisé lutte Maladies Mentales |  75015 PARIS',2.3120565, 48.8407824,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (161,'Centre d Accueil Thérapeutique à temps partiel (C.A.T.T.P.) | CENTRE D ACCUEIL THÉRAPEUTIQUE À TEMPS PARTIEL 75017 PARIS',2.3161394, 48.8833659,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (162,'Etablissement de Soins Chirurgicaux | CLINIQUE OUDINOT - FONDATION SAINT JEAN DE DIEU 75007 PARIS',2.317552, 48.8500847,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (163,'Centre Médico-Psychologique (C.M.P.) | CENTRE MEDICO PSYCHOLOGIQUE DE L UNAFAM (75/G/26) 75017 PARIS',2.3242908, 48.8918435,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (164,'Etablissement de Convalescence et de Repos |  75017 PARIS',2.3246893, 48.8935347,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (165,'Centre Hospitalier Spécialisé lutte Maladies Mentales |  75017 PARIS',2.3247595, 48.8917305,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (166,'Centre Postcure Malades Mentaux | UNITE D ACCUEIL ET DE PSYCHOTHERAPIE FAMILIALE 75008 PARIS',2.3255331, 48.8793965,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (167,'Centre Hospitalier Spécialisé lutte Maladies Mentales | UNITE HOSPITALISATION BICHAT MAISON BLANCHE 75018 PARIS',2.329088, 48.8978746,5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (168,'Etablissement de Soins Chirurgicaux |  75014 PARIS',2.3294242, 48.8263439,5);




--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (1, 'admin@admin', 'ADMIN', 'ADMIN','$2a$10$ec7.4V9cyhghpgPQ7A0WqexswiKDDx0gzGyDAvqaPMFieyH3xRLNe', 'ADMIN'); --pwd=admin
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (2, 'editor@editor', 'EDITOR', 'EDITOR','$2a$10$teFUCDfBqC66ON6DZUwRj./0kjDZjgJ1o9gEeaoBdGmIRmrILs0JO', 'EDITOR'); --pwd=editor
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (3, 'user@user', 'USER', 'USER','$2a$10$oBPVkXPJJGsQUPmQko.Zmeh4aQV3S7I3VUgR8nHJ.9JSfL.7qhmDy', 'USER'); --pwd=user
--

--
--   set sequences
--


ALTER SEQUENCE category_seq RESTART WITH 5;

ALTER SEQUENCE layer_seq RESTART WITH 6;

ALTER SEQUENCE poi_seq RESTART WITH 169;

ALTER SEQUENCE users_seq RESTART WITH 14;