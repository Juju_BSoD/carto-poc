package fr.ngms.cartopoc.models.responses.basic;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.AttributeData;
import fr.ngms.cartopoc.models.responses.IResponsePayload;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classic model class to parse Json. Representing payload of a POI.
 */
public class BasicPOIPayload implements IResponsePayload {
  @JsonProperty("id")
  private final long id;

  @JsonProperty("posX")
  private final double x;

  @JsonProperty("name")
  private final String name;

  @JsonProperty("posY")
  private final double y;

  @JsonProperty("attributesData")
  private final Map<String, String> properties;

  //use for tests
  BasicPOIPayload() {
    this.id = -1;
    this.name = null;
    this.x = 0;
    this.y = 0;
    this.properties = null;
  }

  private BasicPOIPayload(long id, String name, double x, double y, Map<String, String> properties) {
    this.id = id;
    this.x = x;
    this.name = name;
    this.y = y;
    this.properties = properties;
  }

    /**
     * Static method that create a {@link BasicPOIPayload}
     * @param id id of the POI
     * @param name name of the POI
     * @param posX longitude of the POI
     * @param posY latitude of the POI
     * @param attributeData Attributes data of the POI
     * @return a {@link BasicPOIPayload}
     */
  public static BasicPOIPayload from(long id, String name, double posX, double posY, List<AttributeData> attributeData) {
    var map = new HashMap<String, String>();
    attributeData.forEach(e -> map.put(e.getKey(), e.getValue()));
    return new BasicPOIPayload(id, name, posX, posY, map);
  }

  public String getName() {
    return name;
  }

  public Map<String, String> getProperties() {
    return properties;
  }

  public double getY() {
    return y;
  }

  public double getX() {
    return x;
  }

  public long getId() {
    return id;
  }
}
