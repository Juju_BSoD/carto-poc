package fr.ngms.cartopoc.models.resquests;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Basic model class, for Json parsing, that use for import POI in base64 encoded file
 */

public class POIImportRequest {
    @NotNull
    @NotEmpty
    @NotBlank
    @JsonProperty("file64Encoded")
    private String file64Encoded;

    public POIImportRequest(){
        //default constructor
    }
    public String getFile64Encoded() {
        return file64Encoded;
    }

    public void setFile64Encoded(String file64Encoded) {
        this.file64Encoded = file64Encoded;
    }

}
