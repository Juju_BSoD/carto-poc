package fr.ngms.cartopoc.models.geocoding;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class model class, representing a inverse geocoding payload
 */
public class GeoCodingIndirectPayload implements GeocodingPayload{
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 50)
    @JsonProperty("lat")
    private String lat;

    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 50)
    @JsonProperty("lon")
    private String lon;
    private static final String URL = "/reverse.php?";
    public GeoCodingIndirectPayload(){
        //Necessary for parsing
    }

    public void setLat(String latitude) {
        this.lat = latitude;
    }

    public void setLon(String longitude) {
        this.lon = longitude;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }



    @Override
    public String getPayload() {
        return URL+"lat="+lat+"&lon="+lon;
    }

}
