package fr.ngms.cartopoc.models.geocoding;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class model class, representing a direct geocoding payload
 */
public class GeoCodingDirectPayload implements GeocodingPayload{
    private static final String URL = "/search.php?q=";

    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 255)
    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private Address address;


    public GeoCodingDirectPayload(){
        //Necessary for parsing
    }

    public void setName(String search) {
        this.name = search;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Return the given payload of the model
     * @return the string value of the payload
     */
    @Override
    public String getPayload() {
        return URL+name;
    }

}
