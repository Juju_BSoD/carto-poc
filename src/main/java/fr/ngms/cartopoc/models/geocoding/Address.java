package fr.ngms.cartopoc.models.geocoding;

/**
 * Classic Address model class, to parse with Json
 */
public class Address {
  private String road;
  private String city;
  private String state;
  private String postcode;
  private String country;

  public Address() {
    // default constructor
  }

  public String getRoad() {
    return road;
  }

  public void setRoad(String road) {
    this.road = road;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }
}
