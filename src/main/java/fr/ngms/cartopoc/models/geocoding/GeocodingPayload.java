package fr.ngms.cartopoc.models.geocoding;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface GeocodingPayload {
   @JsonIgnore
   String getPayload();
}
