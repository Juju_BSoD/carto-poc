package fr.ngms.cartopoc.models.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Model used to create and update a user
 */
public class UserInformationRequest {
  @NotNull
  @NotEmpty
  @NotBlank
  @Email
  private String email;

  @NotNull
  @NotEmpty
  @NotBlank
  private String password;

  @NotNull
  @NotEmpty
  @NotBlank
  private String firstname;

  @NotNull
  @NotEmpty
  @NotBlank
  private String lastname;

  @NotNull
  @NotEmpty
  @NotBlank
  private String role;

  public UserInformationRequest() {
    //Empty constructor
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public String getRole() {
    return role;
  }

  public String toErrorMessage() {
    var builder = new StringBuilder("fields: ");
    if (email == null || email.isEmpty() || email.isBlank()) {
      builder.append("email").append(" , ");
    }

    if (password == null || password.isEmpty() || password.isBlank()) {
      builder.append("password ");
    }

    if (firstname == null || firstname.isEmpty() || firstname.isBlank()) {
      builder.append("firstname ");
    }

    if (lastname == null || lastname.isEmpty() || lastname.isBlank()) {
      builder.append("lastname ");
    }

    if (role == null || role.isEmpty() || role.isBlank()) {
      builder.append("role ");
    }

    builder.append("missing");
    return builder.toString();
  }
}
