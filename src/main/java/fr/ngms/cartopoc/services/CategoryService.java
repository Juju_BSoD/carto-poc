package fr.ngms.cartopoc.services;


import fr.ngms.cartopoc.helpers.ImageConverter;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.responses.basic.BasicCategoryPayload;
import fr.ngms.cartopoc.models.responses.basic.BasicCategoryPayload.CategoryInformation;
import fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload;
import fr.ngms.cartopoc.models.resquests.CategoryCreationRequest;
import fr.ngms.cartopoc.models.resquests.LayerInformationRequest;
import fr.ngms.cartopoc.orm.entities.CategoryEntity;
import fr.ngms.cartopoc.orm.entities.LayerEntity;
import fr.ngms.cartopoc.orm.repositories.CategoryRepository;
import fr.ngms.cartopoc.orm.repositories.LayerRepository;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.hibernate.HibernateException;
import org.hibernate.PersistentObjectException;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Comparator;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class represent the service of a category. This class deal with the {@link CategoryRepository} to manage category.
 * This class is the interface between {@link fr.ngms.cartopoc.endpoints.CategoryController} and {@link CategoryRepository}.
 */
@ApplicationScoped
public class CategoryService {
  @ConfigProperty(name = "fr.ngms.other.category.name")
  private String otherCategoryName;
  @ConfigProperty(name = "fr.ngms.other.category.description")
  private String otherCategoryDescription;
  private final CategoryRepository categoryRepository;
  private final LayerRepository layerRepository;
  private final Logger logger = Logger.getLogger(getClass().getName());

    /**
     * Basic not found message
     * @param id id
     * @return the message
     */
  private static String notFoundMessage(long id) {
    return "Category with id (" + id + ") not exist";
  }

  @Inject
  public CategoryService(CategoryRepository categoryRepository,LayerRepository layerRepository) {
    this.categoryRepository = categoryRepository;
    this.layerRepository = layerRepository;
  }

    /**
     * Return all categories present into the database.
     * @return the basic {@link ResponseBody} object, with a {@link BasicCategoryPayload} into it payload
     */
  public ResponseBody getAllCategories() {
    var categories = categoryRepository
      .findAll()
      .stream().map(CategoryInformation::from)
            .sorted(Comparator.comparing(cat -> cat.getName().toUpperCase()))
      .collect(Collectors.toList());
      var otherCat = categories
              .stream()
              .filter(cat -> cat.getName().equalsIgnoreCase(this.otherCategoryName)).findFirst();
      if(otherCat.isPresent()){
          categories.remove(otherCat.get());
          categories.add(otherCat.get());
      }

      return new ResponseBody(new BasicCategoryPayload(categories));
  }

    /**
     * This method return a category, find by the given id.
     * @param id the id to find category
     * @return the classic {@link ResponseBody} with for it payload a {@link CategoryInformation} object.
     * If category not found, return a {@link ResponseBody} with notFoundMessage
     */
  public ResponseBody findPoiCategoryById(long id) {
    var targetCategory = categoryRepository.findById(id);
    if (targetCategory == null) {
      return new ResponseBody(notFoundMessage(id));
    }
    return new ResponseBody(CategoryInformation.from(targetCategory));
  }

    /**
     * This method create a category. Only if the category does not exist, and it is name is not equals
     * with name of "Other" category.
     * @param request the category {@link CategoryCreationRequest} to create
     * @return a {@link ResponseBody}, with {@link CategoryInformation} if success, with only a message otherwise
     */
  @Transactional
  public ResponseBody createPoiCategoryEntity(CategoryCreationRequest request) {
    try {
      var isCategoryExist = categoryRepository.findWithoutSensitiveCase(request).isPresent();

      if (isCategoryExist) {
        return new ResponseBody("Category with name (" + request.getName() + ") already exist");
      }
      var result = categoryRepository.insert(CategoryEntity.fromRequest(request));
      return new ResponseBody(CategoryInformation.from(result));
    } catch (PersistentObjectException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }

    /**
     * This method delete a category. The category is find by the given id. This method will not delete the "Other"
     * category. When a category is deleted, all layers attached will be switched to "Other" category
     * @param id the id of the category to delete
     * @return a {@link ResponseBody} with true if success, false otherwise
     */
  @Transactional
  public ResponseBody deleteCategory(long id) {
    try {
      var targetCategory = categoryRepository.findById(id);
      if (targetCategory == null) {
        return new ResponseBody(notFoundMessage(id));
      }
      if(targetCategory.getName().equals(otherCategoryName)){
          return new ResponseBody("You cannot delete OTHER category");
      }
      if(!targetCategory.getLayers().isEmpty()){
          var optOtherCategory = categoryRepository.find("name",otherCategoryName).firstResultOptional();
          if(optOtherCategory.isEmpty()){
              optOtherCategory = Optional.of(categoryRepository.insert(new CategoryEntity(otherCategoryName,otherCategoryDescription)));
          }
          var otherCategory = optOtherCategory.get();
          targetCategory.getLayers().forEach(layer -> {
              otherCategory.addLayer(layer);
              layer.setCategory(otherCategory);
          });

          this.categoryRepository.persistAndFlush(otherCategory);
      }
      categoryRepository.delete(targetCategory);
      return new ResponseBody(true);
    } catch (HibernateException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }

    /**
     * This method update a category. If the category to update
     * @param id
     * @param request
     * @return
     */
  @Transactional
  public ResponseBody updateCategory(long id, CategoryCreationRequest request) {
    try {
      var targetCategory = categoryRepository.findById(id);
      if (targetCategory == null) {
        return new ResponseBody(notFoundMessage(id));
      }
      if(request.getName().equals(this.otherCategoryName)){
        return new ResponseBody("Cannot update " + this.otherCategoryName + " category");
      }
      if(!request.getName().equals(targetCategory.getName())
              && categoryRepository.findWithoutSensitiveCase(request).isPresent()){
          return new ResponseBody("Cannot update " + targetCategory.getName() + " category");
      }
      targetCategory.setDescription(request.getDescription());
      targetCategory.setName(request.getName());
      categoryRepository.persistAndFlush(targetCategory);
      return new ResponseBody(CategoryInformation.from(targetCategory));
    } catch (HibernateException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }

    /**
     * This method return all layers of a category.
     * @param id id of the category to get layers
     * @return a {@link ResponseBody} with {@link BasicLayersPayload} if success, with a {@link fr.ngms.cartopoc.helpers.Constants}
     * notFoundMessage otherwise
     */
  public ResponseBody getLayersOfCategory(long id) {
    var targetCategory = categoryRepository.findById(id);
    if (targetCategory == null) {
      return new ResponseBody(notFoundMessage(id));
    }
    var layers = targetCategory.getLayers();
    var result = layers
      .stream()
      .map(LayerEntity::toLayerInformation).sorted(Comparator.comparing(layer -> layer.getName().toUpperCase()))
      .collect(Collectors.toList());
    return new ResponseBody(new BasicLayersPayload(result));
  }
    /**
     * This method attach a layer into a category. In other terms, this method create a layer, and attach it to the
     * given category, find by the given id. If the category contains another layer with the same name, the layer
     * will not be added
     * @param id the id of the category
     * @param request the layer that will be attached to the category
     * @return a {@link ResponseBody} with {@link fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload.LayerInformation}
     * if success, with a message otherwise
     */
  @Transactional
  public ResponseBody attachLayer(long id, LayerInformationRequest request) {
    try {
      var targetCategory = categoryRepository.findById(id);
      if (targetCategory == null) {
        return new ResponseBody(notFoundMessage(id));
      }

      if (targetCategory.containLayer(request)) {
        return new ResponseBody("Category with id: (" + id + ") already have a layer with name");
      }

      var name = request.getName();
      var description = request.getDescription();
      var properties = String.join(";", request.getProperties());
      var icon = (request.getIcon() != null) ?  ImageConverter.convertImage(request.getIcon()) : null;
      var newLayerEntity = new LayerEntity(name, description, properties, icon);
      targetCategory.addLayer(newLayerEntity);
      newLayerEntity.setCategory(targetCategory);

      categoryRepository.persistAndFlush(targetCategory);

      var reFetchCategory = categoryRepository.findById(id);
      var layers = reFetchCategory.getLayers();
      var lastAddLayer = layers.stream()
        .reduce((a, b) -> a.getId() > b.getId() ? a : b)
        .orElse(targetCategory.getLayers().get(layers.size() - 1));

      return new ResponseBody(BasicLayersPayload.LayerInformation.fromPOILayerEntity(lastAddLayer));
    } catch (HibernateException | IOException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }
    /**
     * This method update the category of a layer. In other terms,
     * this method attach the given layer to an other category.
     * @param idCurrentCategory id of the current category wich layer is attached
     * @param layer layer to update
     * @param idCategoryToAttach id of the category wich layer will be attached
     * @return a {@link ResponseBody} with {@link BasicLayersPayload} if success, a message otherwise
     */
  @Transactional
  public ResponseBody attachLayerOtherCategoryTo(long idCurrentCategory, long idCategoryToAttach, BasicLayersPayload.LayerInformation layer ){
    try {
        var currentCat = this.categoryRepository.findById(idCurrentCategory);
        var targetCat = this.categoryRepository.findById(idCategoryToAttach);
        if(currentCat == null ){
            return new ResponseBody(notFoundMessage(idCurrentCategory));
        }
        if(targetCat == null){
            return new ResponseBody(notFoundMessage(idCategoryToAttach));
        }
        if(!currentCat.getName().equals(this.otherCategoryName)){
            return new ResponseBody("Cannot update Category of layer if category is not "+this.otherCategoryName);
        }
        var layerEntitty = this.layerRepository.findById(layer.getId());
        if(layerEntitty == null ){
            return new ResponseBody(notFoundMessage(layer.getId()));
        }
        var layers = currentCat.getLayers();
        layers.remove(layerEntitty);
        currentCat.setLayers(layers);
        this.categoryRepository.persistAndFlush(currentCat);
        targetCat.getLayers().add(layerEntitty);
        layerEntitty.setCategory(targetCat);
        this.layerRepository.persist(layerEntitty);
        this.categoryRepository.persistAndFlush(targetCat);
        return new ResponseBody(BasicLayersPayload.fromPOILayerEntity(layerEntitty));
    }catch (HibernateException e ){
        logger.severe(e.getMessage());
        return new ResponseBody(e.getMessage());
      }
  }

}
