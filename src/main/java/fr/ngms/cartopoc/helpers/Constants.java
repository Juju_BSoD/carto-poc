package fr.ngms.cartopoc.helpers;

/**
 * Constant class
 */
public class Constants {
  public static final String REQUEST_NOT_NULL = "request can't not be null";
  public static final String UNKNOWN_USER = "Unknown user, please sign up";
  private Constants() {
    //default constructor
  }
}
