package fr.ngms.cartopoc.helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ngms.cartopoc.models.AttributeData;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Helper class to parse attribute data of layer and POI
 */
public class AttributeDataHelper {
  private AttributeDataHelper(){
    //private constructor
  }

    /**
     * Parse properties of a layer to {@link AttributeData} object.
     * @param properties properties of a GeoJson layer
     * @return a {@link List} of {@link AttributeData}
     */
  public static List<AttributeData> parse(String properties) {
    if (properties == null || properties.isBlank() || properties.isEmpty()) {
      return new ArrayList<>();
    }
    try {
      var objectMapper = new ObjectMapper();
      AttributeData[] attributeData = objectMapper.readValue(properties, AttributeData[].class);
      return new ArrayList<>(Arrays.asList(attributeData));
    } catch (JsonProcessingException e) {
      throw new AssertionError(e.getMessage());
    }
  }

    /**
     * Parse a {@link Map} of layer properties, to Json.
     * @param properties GeoJson properties
     * @return a String in Json format
     */
  public static String toJson(Map<String, Object> properties) {
    if (properties.isEmpty()) {
      return "";
    }

    try {
      var values = properties
        .entrySet()
        .stream()
        .map(entry -> new AttributeData(entry.getKey(), (String) entry.getValue()))
        .collect(Collectors.toList());

      var out = new ByteArrayOutputStream();
      var mapper = new ObjectMapper();
      mapper.writeValue(out, values);
      return out.toString();
    } catch (IOException e) {
      throw new AssertionError(e.getMessage());
    }
  }
}
