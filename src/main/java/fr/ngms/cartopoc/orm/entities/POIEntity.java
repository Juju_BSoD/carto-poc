package fr.ngms.cartopoc.orm.entities;

import fr.ngms.cartopoc.helpers.AttributeDataHelper;
import fr.ngms.cartopoc.models.resquests.POIInsertionRequest;

import javax.persistence.*;
import java.util.Objects;

/**
 * This class represent a POI entity according to the database schema
 */
@Entity(name = "POIEntity")
@Table(name = "ngms_poi")
public class POIEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "poi_generator")
  @SequenceGenerator(name = "poi_generator", sequenceName = "poi_seq", allocationSize = 1)
  private long id;

  @Column(name = "posX")
  private double posX;

  @Column(name = "posY")
  private double posY;

  @Column(name = "name")
  private String name;

  @Column(name = "attribute_data_json", columnDefinition = "TEXT")
  private String properties;

  @ManyToOne(fetch = FetchType.LAZY)
  private LayerEntity layer;

  public POIEntity() {
  }

  private POIEntity(String name, double posX, double posY, String properties) {
    this.name = name;
    this.posX = posX;
    this.posY = posY;
    this.properties = properties;
  }

    /**
     * This method create a {@link POIEntity} from a request with {@link fr.ngms.cartopoc.models.resquests.POIInsertionRequest.POIRequest} object
     * @param request POI request {@link fr.ngms.cartopoc.models.resquests.POIInsertionRequest.POIRequest}
     * @return the {@link POIEntity}
     */
  public static POIEntity fromRequest(POIInsertionRequest.POIRequest request) {
    var propertiesJson = AttributeDataHelper.toJson(request.getProperties());
    return new POIEntity(request.getName(), request.getX(), request.getY(), propertiesJson);
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public double getPosX() {
    return posX;
  }

  public void setPosX(double posX) {
    this.posX = posX;
  }

  public double getPosY() {
    return posY;
  }

  public void setPosY(double posY) {
    this.posY = posY;
  }

  public String getProperties() {
    return properties;
  }

  public void setProperties(String properties) {
    this.properties = properties;
  }

  public LayerEntity getLayer() {
    return layer;
  }

  public void setLayer(LayerEntity layer) {
    this.layer = layer;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof POIEntity)) {
      return false;
    }
    POIEntity p = (POIEntity) o;
    return p.posX == posX && p.posY == posY;
  }

  @Override
  public int hashCode() {
    return Objects.hash(posX, posY);
  }

  @Override
  public String toString() {
    return "POIEntity{" +
      "id=" + id +
      ", posX=" + posX +
      ", posY=" + posY +
      ", attributeData='" + properties + '\'' +
      '}';
  }
}