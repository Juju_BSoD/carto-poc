package fr.ngms.cartopoc.orm.entities;

import io.quarkus.security.jpa.Password;
import io.quarkus.security.jpa.Roles;
import io.quarkus.security.jpa.UserDefinition;
import io.quarkus.security.jpa.Username;

import javax.persistence.*;
import javax.ws.rs.DefaultValue;

import org.mindrot.jbcrypt.BCrypt;

/**
 * This class represent a User entity according to the database schema
 */
@Entity(name = "UserEntity")
@Table(name = "ngms_users")
@UserDefinition
public class UserEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_generator")
  @SequenceGenerator(name = "users_generator", sequenceName = "users_seq", allocationSize = 1)
  public long id;

  @Column(name = "password")
  @Password
  private String password;

  @Username
  @Column(name = "email", unique = true)
  private String email;

  @Column(name = "firstname")
  private String firstname;

  @Column(name = "lastname")
  private String lastname;

  @Roles
  @DefaultValue("USER")
  @Column(name = "role")
  private String role;

  public UserEntity() {
  }

  @Override
  public String toString() {
    return "entity : " + id + " " + email + " " + password + " " + firstname + " " + lastname + " " + role;
  }

  public UserEntity(String email, String password, String firstname, String lastname) {
    this.email = email;
    this.password = BCrypt.hashpw(password,BCrypt.gensalt(15));
    this.firstname = firstname;
    this.lastname = lastname;
    this.role = "USER";
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = BCrypt.hashpw(password,BCrypt.gensalt(15));
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getRole() {
    return role;
  }
}
