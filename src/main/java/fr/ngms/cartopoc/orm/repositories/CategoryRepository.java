package fr.ngms.cartopoc.orm.repositories;

import fr.ngms.cartopoc.orm.entities.CategoryEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import fr.ngms.cartopoc.models.resquests.CategoryCreationRequest;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

/**
 * This class represent a category orm repository according to the repository pattern
 */
@ApplicationScoped
public class CategoryRepository implements PanacheRepository<CategoryEntity> {
    /**
     * This method insert the category entity into the database
     * @param entity the category entity {@link CategoryEntity}
     * @return the last {@link CategoryEntity} inserted
     */
  public CategoryEntity insert(CategoryEntity entity) {
    persistAndFlush(entity);
    long maxId = getEntityManager()
      .createQuery("SELECT MAX(p.id) FROM CategoryEntity p", Long.class)
      .getSingleResult();
    return findById(maxId);
  }

    /**
     * This method find without sensitive case if database contains a category
     * @param request the category to find {@link CategoryCreationRequest}
     * @return an {@link Optional<CategoryEntity>}, or {@link Optional} empty
     */
  public Optional<CategoryEntity> findWithoutSensitiveCase(CategoryCreationRequest request) {
    try {
      CategoryEntity e = getEntityManager()
        .createQuery("FROM CategoryEntity AS ent WHERE LOWER(ent.name) = :name", CategoryEntity.class)
        .setParameter("name", request.getName().toLowerCase().trim())
        .getSingleResult();
      return Optional.of(e);
    } catch (NoResultException | NonUniqueResultException exception) {
      return Optional.empty();
    }
  }
}
