package fr.ngms.cartopoc.endpoints;

import fr.ngms.cartopoc.helpers.Constants;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.responses.basic.BasicPOIPayload;
import fr.ngms.cartopoc.models.resquests.POIInsertionRequest;
import fr.ngms.cartopoc.services.POIService;
import fr.ngms.cartopoc.services.UserService;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.logging.Logger;

import static fr.ngms.cartopoc.helpers.PreCondition.idIsNegative;

/**
 * This class is the api endpoint to manage POI. This class expose endpoint for update and delete POI.
 */
@Path("/poi")
@Produces(MediaType.APPLICATION_JSON)
public class POIController {
    private final POIService service;
    private final UserService userService;
    private final Validator validator;
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Inject
    public POIController(POIService service, Validator validator, UserService userService) {
        this.service = service;
        this.validator = validator;
        this.userService = userService;
    }

    /**
     * Api endpoint for update a POI.
     * @param id id of POI to update
     * @param request information of the POI
     * @param context
     * @return the updated POI
     */
    @APIResponse(description = "Return the modified POI", content = @Content(
            schema = @Schema(
                    implementation = BasicPOIPayload.class
            )
    ))
    @RolesAllowed({"ADMIN","EDITOR"})
    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatePOI(@PathParam("id") long id, POIInsertionRequest.POIRequest request, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (idIsNegative(id)) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Id of POI cannot be negative").build();
            }

            if (request == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Poi information cannot be blank or empty").build();
            }

            var response = service.updatePOI(id, request);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * Api endpoint to delete a POI.
     * @param id id of the POI to delete
     * @param context
     * @return true if the POI was deleting, false otherwise
     */
    @APIResponse(description = "Return a basic response type", content = @Content(
            schema = @Schema(
                    implementation = ResponseBody.class
            )
    ))
    @RolesAllowed({"ADMIN","EDITOR"})
    @DELETE
    @Path("/delete/{id}")
    public Response deletePOI(@PathParam("id") long id, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (idIsNegative(id)) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Id of POI cannot be negative").build();
            }
            var response = service.deletePOI(id);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }
}