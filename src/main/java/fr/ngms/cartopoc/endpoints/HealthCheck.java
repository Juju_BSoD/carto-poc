package fr.ngms.cartopoc.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Api status
 */
@Path("/health")
@Produces(MediaType.APPLICATION_JSON)
public class HealthCheck {

    /**
     * Api endpoit to check the status of the api.
     * @return
     */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public Response status() {
    return Response
      .ok()
      .entity("alive")
      .build();
  }
}
