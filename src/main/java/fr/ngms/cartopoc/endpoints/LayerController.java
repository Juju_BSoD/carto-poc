package fr.ngms.cartopoc.endpoints;

import fr.ngms.cartopoc.helpers.Constants;
import fr.ngms.cartopoc.helpers.PreCondition;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload;
import fr.ngms.cartopoc.models.responses.geojson.LayerGeoJson;
import fr.ngms.cartopoc.models.resquests.LayerInformationRequest;
import fr.ngms.cartopoc.models.resquests.POIInsertionRequest;
import fr.ngms.cartopoc.models.resquests.POIImportRequest;
import fr.ngms.cartopoc.services.LayerService;
import fr.ngms.cartopoc.services.UserService;
import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

import static fr.ngms.cartopoc.helpers.PreCondition.idIsNegative;

/**
 * This class is the api endpoint to managing layers. This class has endpoint to create, update, delate, import and
 * export layers. This class has also endpoint to create and attach poi to layers.
 */
@Path("/layers")
@Produces(MediaType.APPLICATION_JSON)
public class LayerController {
    private final LayerService service;
    private final UserService userService;
    private final Validator validator;
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static final String MESSAGE_ID_ERROR = "Id of layer cannot be negative";

    @Inject
    public LayerController(LayerService service, Validator validator, UserService userService) {
        this.service = service;
        this.validator = validator;
        this.userService = userService;
    }

    /**
     * Api endpoint that return a specified layer, find by an id.
     * @param id id to find the layer.
     * @param context
     * @return a layer
     */
    @APIResponse(description = "Return a layer with its basic informations", content = @Content(
            schema = @Schema(implementation = BasicLayersPayload.LayerInformation.class)
    ))
    @Authenticated
    @GET
    @Path("/{id}")
    public Response findLayerById(@PathParam("id") long id, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (PreCondition.idIsNegative(id)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(MESSAGE_ID_ERROR).build();
            }
            var response = service.findById(id);

            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Response.Status.NOT_FOUND).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * Api endpoint to get all POIS of a layer.
     * @param id id of the layer
     * @param context
     * @return all POI'S of the layer, in GeoJson format
     */
    @APIResponse(description = "Return a layer with its basic informations", content = @Content(
            schema = @Schema(implementation = LayerGeoJson.class)
    ))
    @Authenticated
    @GET
    @Path("/{id}/pois")
    public Response findLayerPOIById(@PathParam("id") long id, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (PreCondition.idIsNegative(id)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(MESSAGE_ID_ERROR).build();
            }
            var response = service.findLayerPOIById(id);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * Api endpoint to attach a POI to a layer.
     * @param id id of the layer
     * @param request the information of the POI
     * @param context
     * @return all POI'S of the given layer
     */
    @APIResponse(description = "Return a layer with its basic informations", content = @Content(
            schema = @Schema(
                    implementation = LayerGeoJson.class
            )
    ))
    @RolesAllowed({"EDITOR", "ADMIN"})
    @POST
    @Path("/{id}/attachpoi")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response attachPOI(@PathParam("id") long id, POIInsertionRequest request, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (request == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            var response = service.attachPOIAtLayer(id, request);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.toString());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * Api endpoint to update a layer
     * @param id id of layer to update
     * @param request updated information of the layer
     * @param context
     * @return the updated layer
     */
    @APIResponse(description = "Return a layer with its basic informations", content = @Content(
            schema = @Schema(implementation = BasicLayersPayload.LayerInformation.class)
    ))
    @RolesAllowed({"EDITOR", "ADMIN"})
    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateLayer(@PathParam("id") long id, LayerInformationRequest request, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (request == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            var response = service.updateLayerInformation(id, request);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.toString());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * Api endpoint to delete a layer.
     * @param id id of layer to delete
     * @param context
     * @return true if the layer was deleted, false otherwise
     */
    @RolesAllowed({"ADMIN"})
    @DELETE
    @Path("/delete/{id}")
    public Response deleteLayer(@PathParam("id") long id, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (idIsNegative(id)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(MESSAGE_ID_ERROR).build();
            }
            var response = service.deleteLayer(id);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * Api endpoint to export a layer.
     * @param id id of the given layer to export
     * @param context
     * @return the exported layer encoded in base64 format
     */
    @RolesAllowed({"EDITOR", "ADMIN"})
    @GET
    @Path("/export/{id}")
    public Response exportLayer(@PathParam("id") long id, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (idIsNegative(id)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(MESSAGE_ID_ERROR).build();
            }
            var layer = service.findById(id);
            if(!layer.isSuccess() ){
                return Response.status(Response.Status.BAD_REQUEST).entity(layer).build();
            }
            var response = service.exportLayer((BasicLayersPayload.LayerInformation) layer.getPayload());
            if (!response.isSuccess()) {
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(response).build();
            }
            if (response.isSuccess()) {
                Files.delete(Paths.get(((BasicLayersPayload.LayerInformation) layer.getPayload()).getName() + ".geojson"));
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * Api endpoint to import POI into a layer
     * @param id id of layer
     * @param layer encoded GeoJson file in base64 Format
     * @param context
     * @return
     */
    @APIResponse(description = "Return a layer with its basic informations and its poi attached in format GeoJson", content = @Content(
            schema = @Schema(
                    implementation = LayerGeoJson.class
            )
    ))

    @RolesAllowed({"EDITOR", "ADMIN"})
    @POST
    @Path("/{id}/importPois")
    public Response importLayer(@Valid @PathParam("id") long id, POIImportRequest layer, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (layer == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }
            var violations = validator.validate(layer);
            if (!violations.isEmpty()) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            var response = service.importPoi(id, layer);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

}
