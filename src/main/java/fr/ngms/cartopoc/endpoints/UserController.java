package fr.ngms.cartopoc.endpoints;

import fr.ngms.cartopoc.helpers.Constants;
import fr.ngms.cartopoc.helpers.PreCondition;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.user.*;
import fr.ngms.cartopoc.services.UserService;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import java.util.logging.Logger;

/**
 * This class is the api endpoint to manage user. CRUD, and login/authentification operations are implemented.
 */

@Path("/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController {
    private final UserService userService;
    private final Validator validator;
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Inject
    public UserController(UserService userService, Validator validator) {
        this.userService = userService;
        this.validator = validator;
    }

    /**
     * Api endpoint that sign in a user
     * @param request user login information
     * @return a JWT token if the user is known, error otherwise.
     */
    @POST
    @Path("/login")
    @APIResponse(description = "Perform login for a user and return a token. You have to include this Authorization " +
            "bearer token into the HTTP Headers to connect with the api.",
            content = @Content(schema = @Schema(implementation = AuthResponse.class))
    )
    public Response login(UserLoginRequest request) {
        try {
            if (request == null) {
                return Response.status(Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                return Response.status(Status.BAD_REQUEST).entity(new ResponseBody(violations.toString())).build();
            }
            var response = userService.login(request);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.UNAUTHORIZED).entity(response).build();
            }

            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Endpoint for register/create a user.
     * @param request information of the new user
     * @return user information
     */
    @POST
    @Path("/create")
    @Parameter(schema = @Schema(implementation = UserInformationRequest.class))
    @APIResponse(description = "Register a new user",
            content = @Content(schema = @Schema(implementation = UserInformation.class))
    )
    public Response signUp(UserInformationRequest request) {
        try {
            if (request == null) {
                return Response.status(Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                return Response.status(Status.BAD_REQUEST).entity(new ResponseBody(request.toErrorMessage())).build();
            }
            var response = userService.signUp(request);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Endpoint to get all registered users
     * @param context
     * @return all registered users
     */
    @RolesAllowed({"ADMIN"})
    @APIResponse(description = "Gel all users informations",
            content = @Content(schema = @Schema(implementation = UserListResponse.class))
    )
    @GET
    @Path("/all")
    public Response listAll(@Context SecurityContext context) {
        try {
            if (!userService.isKnownUser(context)) {
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            var response = userService.listAll();
            return Response.ok(new ResponseBody(response)).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Endpoint to return a specify user, find by id
     * @param id id of the user to search
     * @param context
     * @return the finding user
     */
    @RolesAllowed({"ADMIN"})
    @GET
    @Path("/{id}")
    public Response findById(@Valid @PathParam("id") long id, @Context SecurityContext context) {
        try {
            if (!userService.isKnownUser(context)) {
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (PreCondition.idIsNegative(id)) {
                return Response.status(Status.BAD_REQUEST).entity("id must be greater than 0 not").build();
            }

            var response = userService.findById(id);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.NOT_FOUND).entity("Unknown user with id (" + id + ")").build();
            }

            return Response.ok(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Endpoint for updating a user
     * @param id id of user to update
     * @param request new information of user
     * @param context
     * @return the updated user
     */
    @RolesAllowed({"ADMIN"})
    @PUT
    @Path("/update/{id}")
    @APIResponse(description = "Update already register user. ",
            content = @Content(schema = @Schema(implementation = UserInformation.class))
    )
    public Response updateUser(@Parameter(required = true) @Valid @PathParam("id") long id,
                               @Parameter(required = true) UserInformationRequest request, @Context SecurityContext context) {
        try {
            if (!userService.isKnownUser(context)) {
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (request == null || context.getUserPrincipal().getName() == null) {
                return Response.status(Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }
            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                return Response.status(Status.BAD_REQUEST).entity(new ResponseBody(request.toErrorMessage())).build();
            }

            var response = userService.updateUser(id, request, context.getUserPrincipal().getName());
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }


    /**
     * Endpoint for deleting a user
     * @param id id of user to delete
     * @param context
     * @return true if user was deleted, false otherwise
     */
    @RolesAllowed("ADMIN")
    @APIResponse(description = "Delete the specified user.")
    @DELETE
    @Path("/delete/{id}")
    public Response deleteById(@Parameter(required = true) @Valid @PathParam("id") long id,
                               @Context SecurityContext context) {
        try {
            if (!userService.isKnownUser(context)) {
                return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
            }
            if (PreCondition.idIsNegative(id)) {
                return Response.status(Status.BAD_REQUEST).entity("id must be greater than 0 not").build();
            }
            if (context.getUserPrincipal().getName() == null) {
                return Response.status(Status.BAD_REQUEST).entity("Mail of requester must be specified").build();
            }
            var response = userService.deleteById(id, context.getUserPrincipal().getName());
            if (!response.isSuccess()) {
                return Response.status(Status.UNAUTHORIZED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }
}
