package fr.ngms.cartopoc.endpoints;

import fr.ngms.cartopoc.helpers.Constants;
import fr.ngms.cartopoc.models.geocoding.GeoCodingDirectPayload;
import fr.ngms.cartopoc.models.geocoding.GeoCodingIndirectPayload;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.services.GeoCodingService;
import fr.ngms.cartopoc.services.UserService;
import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import javax.inject.Inject;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * This class is the api endpoint to use geocoding interface. This class can do direct and inverse geocoding.
 * Note that this endpoint is an interface connected with <a href="https://nominatim.org/"> Nominatim </a>.
 */
@Path("/geocode")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class GeoCodingController {
  private final GeoCodingService geoCodingService;
  private final UserService userService;
  private final Validator validator;
  @Inject
  public GeoCodingController(GeoCodingService geoCodingService, UserService userService, Validator validator) {
    this.geoCodingService = geoCodingService;
    this.userService = userService;
    this.validator = validator;
  }

    /**
     * Api endpoint to make direct geocoding.
     * @param payload direct geocoding payload.
     * @param context
     * @return
     */
  @Authenticated
  @Path("/direct")
  @POST
  public Response directGeocoding(@RequestBody GeoCodingDirectPayload payload, @Context SecurityContext context) {
    try {
        if(!userService.isKnownUser(context)){
            return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
        }
        if(payload == null ){
          return Response.status(Response.Status.BAD_REQUEST).build();
        }
        var valid = validator.validate(payload);
        if(!valid.isEmpty()){
          return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
      return geoCodingService.geocode(payload);
    } catch (Exception e) {
      return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
    }
  }

    /**
     * Api endpoint to make reverse geocoding.
     * @param payload reverse geocoding payload.
     * @param context
     * @return
     */
  @Authenticated
  @Path("/indirect")
  @POST
  public Response indirectGeocoding(@RequestBody GeoCodingIndirectPayload payload, @Context SecurityContext context) {
    try {
        if(!userService.isKnownUser(context)){
            return Response.status(Response.Status.UNAUTHORIZED).entity(Constants.UNKNOWN_USER).build();
        }
      if(payload == null ){
        return Response.status(Response.Status.BAD_REQUEST).build();
      }
      var valid = validator.validate(payload);
      if(!valid.isEmpty()){
        return Response.status(Response.Status.EXPECTATION_FAILED).build();
      }
      return geoCodingService.geocode(payload);
    } catch (Exception e) {
      return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
    }
  }
}
