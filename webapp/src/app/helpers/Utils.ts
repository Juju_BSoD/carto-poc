import { POIGeoJson, POIRequest } from 'dist/openapi';

export class Utils {

  /**
   * Validator to check if string contains whites spaces
   * @param control string to check
   */
  public static noWhitespaceValidator(control: string): boolean{
    if (control === undefined || control == null) {
      return true;
    }
    const isWhitespace = control.trim().length === 0;
    return !isWhitespace;
  }

  /**
   * Transform poi geojson to poi request object
   * @param poi
   */
  public static poiGeoJsonToPoiRequest(poi: POIGeoJson): POIRequest{
    const poiResquest = {} as POIRequest;
    poiResquest.name = poi.properties.name;
    poiResquest.posX = poi.geometry.coordinates[0];
    poiResquest.posY = poi.geometry.coordinates[1];
    const properties = {};
    for (const [key, value] of Object.entries(poi.properties)) {
      if (key === 'id' || key === 'name') { continue; }
      properties[key] = value;
    }
    poiResquest.attributesData = properties;
    return poiResquest;
  }
}



