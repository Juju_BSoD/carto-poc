import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
/**
 * Service to save values in geocoding component
 */
export class StateService {
  state$ = new BehaviorSubject<any>(null);
}
