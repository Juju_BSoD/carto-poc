import { Injectable } from '@angular/core';
import { DefaultService, GeoCodingDirectPayload, GeoCodingIndirectPayload } from "dist/openapi";


@Injectable({
  providedIn: 'root'
})
export class GeocodingService {
  constructor(private api: DefaultService) { }

  /**
   * Call nominatim API to do direct geocoding
   * @param request body with values (city, postal code)
   */
  geocodingDirect(request: GeoCodingDirectPayload){
    return this.api.geocodeDirectPost(request);
  }

  /**
   * Call nominatim API to do reverse geocoding
   * @param request body with values (coordinates)
   */
  geocodingInverse(request: GeoCodingIndirectPayload){
    return this.api.geocodeIndirectPost(request);
  }
}
