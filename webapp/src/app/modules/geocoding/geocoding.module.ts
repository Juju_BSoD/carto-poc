import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GeocodingComponent} from './components/geocoding.component';
import {GeocodingService} from './services/geocoding.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';
import {StateService} from "./services/state.service";


@NgModule({
  declarations: [
    GeocodingComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule
  ],
  providers: [
    GeocodingService,
    StateService
  ]
})
export class GeocodingModule { }
