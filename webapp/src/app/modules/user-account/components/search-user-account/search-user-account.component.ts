import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {ResponseBody, UserInformation, UserListResponse} from 'dist/openapi';
import {ToastrService} from 'ngx-toastr';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {UserService} from '../../service/user.service';
import {Role} from "../../../../models/roles";


@Component({
  selector: 'app-search-user-account',
  templateUrl: './search-user-account.component.html',
  styleUrls: ['./search-user-account.component.scss']
})
export class SearchUserAccountComponent implements OnInit {
  control: FormControl = new FormControl();
  users: UserInformation[];
  filteredUsers: Observable<UserInformation[]>;

  // Inject user & toast service
  constructor(private router: Router, private userService: UserService, private toast: ToastrService) {
    this.users = [];
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  /**
   * Filter values in table following value passed in parameter
   * @param value value to filter
   * @private use only here
   */
  private _filter(value: string): UserInformation[] {
    const filterValue = this._normalizeValue(value);
    return this
      .users
      .filter((user) => this._normalizeValue(user.email).includes(filterValue)
        || this._normalizeValue(user.lastname).includes(filterValue)
        || this._normalizeValue(user.firstname).includes(filterValue)
      );
  }

  /**
   * Send login request as soon as the form is submitted,
   * and redirect user to home page
   */
  ngOnInit(): void {

    // check if user is admin, redirect it to home page otherwise
    if(this.userService.getUserRoles() !== Role.ADMIN) {
      this.router.navigate(['/home']);
    }

    this.userService
      .getAllUsers()
      .subscribe({
        next: (response: ResponseBody) => {
          if (!response.success) {
            this.toast.error('An error occurred while get users: ' + response.message.toString(), 'Error', {
              timeOut: 4000,
              positionClass: 'toast-top-center'
            });
            return;
          }
          let users : UserListResponse = response.payload;
          this.users = users.users;
          this.filteredUsers = this.control.valueChanges
            .pipe(
              startWith(''),
              map(query => this._filter(query))
            );
        },
        error: (error) => {
          this.toast.error('An error occurred while requesting users:  ' + error.message, 'Error', {
            timeOut: 4000,
            positionClass: 'toast-top-center'
          });
          return;
        }
      });
  }

  /**
   * Redirect usr to account page
   * @param user
   */
  goToProfile(user: UserInformation): void {
    const link = ['/account', user.id];
    this.router.navigate(link);
  }
}
