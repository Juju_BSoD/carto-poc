import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserInformationRequest } from 'dist/openapi';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Utils } from 'src/app/helpers/Utils';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-account-authentification',
  templateUrl: './account-authentification.component.html',
  styleUrls: ['./account-authentification.component.scss']
})
export class AccountAuthentificationComponent implements OnInit {

  isValid: boolean;
  isSubmitted: boolean;
  user: UserInformationRequest;
  userForm: FormGroup;
  constructor(private userService: UserService, private formBuilder: FormBuilder,
              private router: Router, private loader: NgxSpinnerService, private toast: ToastrService) {
    // if user don't have token we redirect it to
    if (localStorage.token){
      this.router.navigate(['/home']);
    }
  }

  /**
   * This method is called when accountAuthentification component is initializing.
   * Iniatialize formGroup with initial values
   */
  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      password: ['', Validators.required],
      role: 'USER'
    });
  }

  /**
   * Send sign up request as soon as the form is submitted,
   * and create User object
   */
  onSubmitForm(): void{
    if (!this.userForm.valid){
      this.isValid = false;
      return;
    }
    if(!this._validateAfterSubmit()){
      this.toast.error('Error, you use only whitespace ', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      this.isValid = false;
      return;
    }

    // build user object
    const formvalue = this.userForm.value;
    this.user = {
      email: formvalue.email,
      firstname : formvalue.firstname,
      lastname : formvalue.lastname,
      password : formvalue.password,
      role : 'USER'
    };
    this.userService.signUp(this.user).subscribe({
      next : res => {
        this.isSubmitted = true;
        this.isValid = true;
        this.toast.success('Account created', 'Success', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      },
      error : er => {
        this.isSubmitted = true;
        this.isValid = false;
        this.toast.error('Account not created ' + er, 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });

  }

  /**
   * Doing some validations before call to User API
   * @private use only here
   */
  private _validateAfterSubmit(): boolean{
    let valid = true;
    if(!Utils.noWhitespaceValidator(this.userForm.value.firstname)){
      this.userForm.controls.firstname.setErrors({'incorret': true});
      valid = false;
    }
    if(!Utils.noWhitespaceValidator(this.userForm.value.lastname)){
      this.userForm.controls.lastname.setErrors({'incorret': true});
      valid = false;
    }
    if(!Utils.noWhitespaceValidator(this.userForm.value.password)){
      this.userForm.controls.password.setErrors({'incorret': true});
      valid = false;
    }
    return valid;
  }
}
