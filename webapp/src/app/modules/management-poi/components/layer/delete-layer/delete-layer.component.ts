import {
  Component, EventEmitter,
  Input,
  OnInit, Output,
  ViewChild
} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {LayerInformation} from 'dist/openapi';
import {ToastrService} from 'ngx-toastr';
import {POIService} from '../../../services/poi.service';


@Component({
  selector: 'app-delete-layer',
  templateUrl: './delete-layer.component.html',
  styleUrls: ['./delete-layer.component.scss']
})
export class DeleteLayerComponent implements OnInit {

  addForm: FormGroup;
  @Input() idLayerToDelete: number;
  @Input() nameLayer: string;
  @Input() descriptionLayer: string;
  @Input() numberOfPoiAttachedToLayer: number;
  @Input() layer: LayerInformation;
  @Input() layers: Array<LayerInformation>;
  @Input() index: number;
  @ViewChild('closebutton') closebutton;
  @Output('removeLayerFromMap') removeLayerFromMap: EventEmitter<any> = new EventEmitter();

  // Inject poi & toast service
  constructor(private fb: FormBuilder, private servicePoi: POIService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    if (this.layer == null) {
      return;
    }
  }

  /**
   * Send delete layer request as soon as the form is submitted,
   * and close modal window
   */
  deleteLayer(): void {
    if (this.idLayerToDelete <= 0) {
      return;
    }

    this.servicePoi.deleteLayer(this.layer.id).subscribe({
      next: _ => {
        this.layers.splice(this.index, 1);
        this.toastr.success('Layer ' + this.nameLayer + ' was deleted !', 'Layer Deleted', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.removeLayerFromMap.emit();
      },
      error: _ => {
        this.toastr.error('Layer ' + this.nameLayer + ' was deleted !', 'Layer Deleted', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });
    this.closebutton.nativeElement.click();
  }
}
