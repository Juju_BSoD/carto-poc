import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {
  BasicPOIPayload,
  LayerInformation, POIGeoJson, ResponseBody
} from '../../../../../../../dist/openapi';
import {POIService} from '../../../services/poi.service';
import {ToastrService} from 'ngx-toastr';
import {Utils} from 'src/app/helpers/Utils';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-update-poi',
  templateUrl: './update-poi.component.html',
  styleUrls: ['./update-poi.component.scss']
})
export class UpdatePoiComponent implements OnInit {
  @Input() layer: LayerInformation;
  @Input() poi: POIGeoJson;
  @Input() indexPoi: number;
  @Input() poiList: Array<POIGeoJson>;
  poiLocal: POIGeoJson;
  poiLocalAttribute;
  sizeAttribute: number;
  @ViewChild('closebutton') closebutton;
  @Output('showPoi') showPoi: EventEmitter<any> = new EventEmitter();

  // Inject poi & toast service
  constructor(private servicePoi: POIService, private toastService: ToastrService) {
    this.poiLocal = {
      geometry: {
        coordinates: []
      },
      properties: {},
      type: ''
    };
  }

  /**
   * This method is called when updatePoi component is initializing.
   * Define layer object to null
   */
  ngOnInit(): void {
    if (this.layer == null) {
      return;
    }

  }

  /**
   * This method is call when any data-bound property of a template directive changes
   */
  ngOnChanges(): void {
    this.poiLocal.properties.id = this.poi.properties.id;
    this.poiLocal.properties.name = this.poi.properties.name;
    this.poiLocal.geometry.coordinates[0] = this.poi.geometry.coordinates[0];
    this.poiLocal.geometry.coordinates[1] = this.poi.geometry.coordinates[1];
    this.poiLocalAttribute = this._getPropertiesFromPOI(this.poi);
    this.sizeAttribute = (this.layer.attributes != undefined && this.layer.attributes[0] != '') ? this.layer.attributes.length : 0;
  }

  /**
   * Send update poi request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(updateLayerForm: NgForm): void {
    if (updateLayerForm.invalid) {
      this.toastService.warning('Form is invalid', 'Warning', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    if (!this._validateAfterSubmit(updateLayerForm)) {
      this.toastService.error('Form is invalid, you use only escape', 'Warning', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    const poiRequest = Utils.poiGeoJsonToPoiRequest(this.poiLocal);
    for (const [key] of Object.entries(this.poiLocalAttribute)) {
      if (updateLayerForm.controls[key]){
        this.poiLocalAttribute[key] = updateLayerForm.controls[key].value;
       }
    }
    poiRequest.attributesData = this.poiLocalAttribute;
    this.servicePoi.updatePoi(poiRequest, Number.parseInt(this.poi.properties.id)).subscribe({
      next: (_: ResponseBody) => {
        const updatePoi: BasicPOIPayload = _.payload;
        this.poiList[this.poiList.indexOf(this.poi)] = this._toGeoJson(updatePoi);
        this.showPoi.emit({
          poi: this._toGeoJson(updatePoi),
          index: this.indexPoi,
          poiToDelete: this.poi
        });
        this.toastService.success('POI ' + poiRequest.name + ' [ ' + poiRequest.posX + ', ' +
          poiRequest.posY + '] was updated !', 'POI Updated', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.closebutton.nativeElement.click();

      },
      error: _ => {
        this.toastService.error('POI ' + poiRequest.name + ' was not updated !', 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.closebutton.nativeElement.click();
      }
    });
  }

  /**
   * Doing some validations before call to POI API
   * @param form Form bound with template directive
   * @private use only here
   */
  private _validateAfterSubmit(form: NgForm): boolean {
    if (!Utils.noWhitespaceValidator(form.controls.poiName.value)) {
      form.controls.poiName.setErrors({'incorrect': true});
      form.controls.poiName.reset(this.poi.properties.name);
      return false;
    }
    return true;
  }

  /**
   * Reset form with local values
   * @param form Form bound with template directive
   */
  resetForm(form: NgForm): void {
    this.poiLocal.properties.id = this.poi.properties.id;
    this.poiLocal.properties.name = this.poi.properties.name;
    this.poiLocal.geometry.coordinates[0] = this.poi.geometry.coordinates[0];
    this.poiLocal.geometry.coordinates[1] = this.poi.geometry.coordinates[1];
    for (const [key,value] of Object.entries(this._getPropertiesFromPOI(this.poi))){
      if(form.controls[key]){
        form.controls[key].reset(value);
      }
    }
  }

  /**
   * Get properties from poi passed in parameters
   * @param poi poi payload with values
   * @private use only here
   */
  private _getPropertiesFromPOI(poi: POIGeoJson): any {
    const properties = {};
    for (const [key, value] of Object.entries(poi.properties)) {
      if (key === 'id' || key === 'name') {
        continue;
      }
      properties[key] = value;
    }
    return properties;
  }

  /**
   * Parse poi payload to basic geoJson
   * @param poi poi payload with values
   * @private use only here
   */
  private _toGeoJson(poi: BasicPOIPayload): POIGeoJson {
    const geoJsonPoi: POIGeoJson = {
      geometry: {
        coordinates: [poi.posX, poi.posY],
        type: 'Point'
      },
      properties: {
        id: poi.id + '',
        name: poi.name
      },
      type: 'Feature'
    };
    for (const [key, value] of Object.entries(poi.attributesData)) {
      geoJsonPoi.properties[key] = value;
    }
    return geoJsonPoi;
  }
}
