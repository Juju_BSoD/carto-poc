import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {POIService} from './services/poi.service';
import { AddCategoryComponent } from './components/category/add-category/add-category.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManagementPoiComponent } from './components/poi/management-poi.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AddLayerComponent } from './components/layer/add-layer/add-layer.component';
import { UpdateLayerComponent } from './components/layer/update-layer/update-layer.component';
import { ToastrModule } from 'ngx-toastr';
import { UpdateCategoryComponent } from './components/category/update-category/update-category.component';
import { DeleteCategoryComponent } from './components/category/delete-category/delete-category.component';
import { DeleteLayerComponent } from './components/layer/delete-layer/delete-layer.component';
import { AddPoiComponent } from './components/poi/add-poi/add-poi.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { UpdatePoiComponent } from './components/poi/update-poi/update-poi.component';
import { DeletePoiComponent } from './components/poi/delete-poi/delete-poi.component';
import { AttachOtherCategoryLayerComponent } from './components/layer/attach-other-category-layer/attach-other-category-layer.component';
import { AddSpecificPoiComponent } from './components/poi/add-specific-poi/add-specific-poi.component';
import { ImportPoiComponent } from './components/poi/import-poi/import-poi.component';

@NgModule({
  declarations: [ManagementPoiComponent, AddCategoryComponent, AddLayerComponent,
    UpdateCategoryComponent, UpdateLayerComponent, DeleteCategoryComponent, DeleteLayerComponent, AddPoiComponent, UpdatePoiComponent, DeletePoiComponent, AttachOtherCategoryLayerComponent ,
    AddSpecificPoiComponent,
    ImportPoiComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    NgbPaginationModule,
    NgxPaginationModule
  ],
  exports: [
    ManagementPoiComponent,
    AddCategoryComponent,
    AddLayerComponent
  ],
  providers: [
    POIService
  ]
})
export class ManagementPOIModule {
}
